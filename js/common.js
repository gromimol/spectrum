$(document).ready(function() {

    // slider
    (function() {
        let $slider = $('.slider')
        $slider.each(function() {
            let $slidesCount = $(this).data('slides')
            $(this).slick({
                slidesToShow: $slidesCount,
                swipeToSlide: true,
                prevArrow: '<span class="prev-arrow"><svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7 13L1 7L7 1" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg></span>',
                nextArrow: '<span class="next-arrow"><svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.999998 13L7 7L0.999999 1" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg></span>',
                responsive: [{
                    breakpoint: 767,
                    settings: {
                        arrows: false,
                        slidesToShow: 1,
                        variableWidth: true
                    }
                }]
            })
        })
    })()

    // top slider
    $('.top-slider').slick({
        variableWidth: true,
        swipeToSlide: true,
        prevArrow: '<span class="prev-arrow"><svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7 13L1 7L7 1" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg></span>',
        nextArrow: '<span class="next-arrow"><svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.999998 13L7 7L0.999999 1" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg></span>',
        responsive: [{
            breakpoint: 767,
            settings: {
                arrows: false
            }
        }]

    })

    // tab
    $('.tab-list li').on('click', function() {
        let tabId = $(this).data('tab')
        $(this).addClass('active').siblings().removeClass('active');
        $(this).closest('.tabs-wrapper').find('.tab-content').removeClass('active')
        $(`#${tabId}`).addClass('active')
    })

    // mobile menu 
    $('.mobile-btn').on('click', function() {
        $('body').addClass('open-menu');
    })
    $('.close-menu, #overlay').on('click', function() {
        $('body').removeClass('open-menu');
    })

})